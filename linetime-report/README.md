# Report LineTime

## Cose da fare

### Unity

Su Unity devono essere ancora implementate alcune features, in particolare

* Acquisti in-app
* Pubblicità
* Finire la UI, collegando anche alcune parti che ora non funzionano
* Animazioni varie sulla UI
* Altre features a cui però non hanno ancora pensato, per esempio la gestione degli amici (chissà quante sono queste features?)

Lo sviluppatore di LineTime, Stefano, si sente abbastanza skillato su Unity e mi ha detto che non crede di avere problemi su tutte queste feature. Probabilmente, però, potremmo dovergli dare una mano semplicemente per il fatto che ci sono troppe cose da fare per una persona sola.

Eugenio invece sostiene che secondo lui avranno bisogno di una mano soprattutto per quanto riguarda gli acquisti in-app e le pubblicità, perchè a suo avviso non è semplice come sembra.

### Infrastruttura

Il server è su Amazon Web Services (AWS). Al momento utilizzano un server Node.js su una macchina `m5.large`. L'idea di Stefano è di voler implementare il multi-threading e spostare il server su un cluster per prevenire un eventuale sovraccarico al lancio.

A mio avviso questa operazione, oltre ad essere lunga e dispendiosa, non è necessaria. Ritengo che una macchina large possa sopportare tranquillamente un flusso di utenti relativamente alto per il lancio di un'applicazione, a patto che il codice sia sufficientemente ottimizzato. Si tratta comunque di un'opinione personale che andrebbe avvalorata da dei dati oggettivi.

Ecco un paio di dati

* Il server è direttamente un processo node `node /home/ubuntu/..../main.js` che ascolta sulla porta `8080`. La porta è stata aperta manualmente.
* Non c'è praticamente nient'altro degno di nota su questo server. C'è git e il deploy è fatto accedendo in SSH e facendo un pull. Tutto il resto non è stato neanche configurato.

Le specifiche della macchina:

```
    description: Computer
    product: m5.large
    vendor: Amazon EC2
    serial: ec2f7ed7-ebad-1cdd-6484-b1724ed0f7b5
    width: 64 bits
    capabilities: smbios-2.7 dmi-2.7 smp vsyscall32
    configuration: uuid=D77E2FEC-ADEB-DD1C-6484-B1724ED0F7B5
  *-core
       description: Motherboard
       vendor: Amazon EC2
       physical id: 0
     *-firmware
          description: BIOS
          vendor: Amazon EC2
          physical id: 0
          version: 1.0
          date: 10/16/2017
          size: 64KiB
          capabilities: pci edd acpi virtualmachine
     *-cpu
          description: CPU
          product: Intel(R) Xeon(R) Platinum 8175M CPU @ 2.50GHz
          vendor: Intel Corp.
          physical id: 4
          bus info: cpu@0
          version: Intel(R) Xeon(R) Platinum 8175M CPU @ 2.50GHz
          slot: CPU 0
          size: 2500MHz
          capacity: 3500MHz
          width: 64 bits
          clock: 100MHz
          capabilities: x86-64 fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp constant_tsc rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch invpcid_single pti fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm mpx avx512f avx512dq rdseed adx smap clflushopt clwb avx512cd avx512bw avx512vl xsaveopt xsavec xgetbv1 xsaves ida arat pku ospke
          configuration: cores=1 enabledcores=1 threads=2
     *-memory
          description: System memory
          physical id: 1
          size: 7681MiB
```

### Backend

Il problema maggiore è lato Server. Stefano ha lavorato al codice del server, ma è stata la sua prima esperienza in questo ambito. Per questo motivo alcune cose non sono implementate proprio nella maniera giusta, anzi. Provo ad analizzare alcuni punti.

* Il server è un singolo worker che lavora su una singola macchina. In genere è possibile utilizzare più worker, bisognerebbe capire se è fattibile in questo caso e se porterebbe dei benefici
* Il codice del server è un unico gicantesco script di 2600 righe. Per fortuna però le callback dei singoli eventi dei socket sono ben separate. Inoltre il codice è abbastanza lineare e a tratti ingenuo. Questo è meglio che un codice ingegnerizzato male.
* La comunicazione client-server è esclusivamente gestita tramite socket. Questa cosa potrebbe essere un problema perchè il socket rimane aperto per tutto il tempo in cui l'applicazione sta girando, anche quando non c'è comunicazione con il server. In questo si raggiunge molto più in fretta il massimo numero di connessioni che può supportare il server. Invece, se si avessero delle RESTful API le connessioni verrebbero utilizzate solo per le singole chiamate, che sono molto brevi. Bisogna quindi fare un test per capire quante connessioni può supportare in contemporanea il server, comunicare al cliente questo numero e lasciar decidere a loro se è sufficientemente alto per il numero di utenti che si aspettano di poter servire in contemporanea
* Le eccezioni non sono gestite in nessun modo lato server. Questo significa che quando si verifica un'eccezione il socket si blocca e non può più essere utilizzato. Lato client anche questa cosa non è gestita quindi bisogna riavviare l'app. Bisogna quindi mettere mano a tutte le chiamate del server per gestire le eccezioni.
* È stato fatto un po' di tempo fa uno stress test sul codice del server, sul pezzo di logica che secondo Stefano è il più pesante. Il risultato è stato di 1000 req/s che sono state gestite senza tanti problemi. Questo test andrebbe rifatto sul codice attuale e ci aiuta a capire per quanti utenti la macchina Large sia sufficiente.
* Al momento il codice del server apre una nuova connessione al DB ogni volta che riceve una chiamata all'interno di un socket. Si potrebbe valutare di usare un'unica connessione al DB e riutilizzare sempre la stessa. In tal caso bisognerebbe gestire i casi in cui la connessione si chiude per qualche motivo.
* Alcune funzioni del server soffrono di callback hell. In particolare c'è una funzione di 300 righe con un sacco di callback che è un bordello. Per fortuna contiene del codice che funziona bene e probabilmente non andrà toccato.

Sul back-end quello che mi è stato segnalato come "da fare" è

* Rifare la Leaderboard perchè Stefano sostiene che non sia ottimizzata. Sono d'accordo sull'ottimizzazione ma secondo me bisognerebbe prima fare uno stress test per vedere se effettivamente è un collo di bottiglia, altrimenti si lascia così.
* Nient'altro? Ovviamente non è così, ci saranno un sacco di cose da fare ma non sappiamo quali

### Immagini

Adesso le immagini che vengono utilizzate nell'applicazione sono su server altrui. Bisogna:

* Creare uno script che faccia lo scraping automatico di tutte le immagini. Lo script deve anche riportare quali sono le immagini che non sono state scaricate perchè magari non esistono più
* Andare a recuperare le immagini di cui è fallito il download (a mano)
* Croppare le immagini rendendole quadrate (a mano)
* Creare uno script che elabori automaticamente le immagini in modo da dargli la risoluzione giusta (512x512?), applichi degli effetti per renderle tutte simili tra di loro e le carichi sul server S3, modificando gli URL. Però bisogna fare attenzione:
    * C'è un file XLSX che contiene tutti gli eventi che possono essere giocati (tutte le carte) con anche gli URL delle immagini. Al momento c'è uno script che legge l'excel, distrugge il documento sul DB e lo ricrea da zero. Questo script viene fatto girare di rado.
    * Il problema principale è che nel frattempo sono state fatte delle modifiche direttamente sul DB quindi l'excel non è più aggiornato. Bisogna fare un matching delle due fonti e riportare quello che manca nell'excel
    * Lo script che modifica le immagini dovrebbe modificare prima di tutto gli URL sull'excel. Poi bisognerebbe far girare lo script che trascrive l'excel sul DB
* Al momento non c'è un server S3. Bisogna tirarne su uno, vedendo anche i prezzi.
    * Una stima dei prezzi, considerando 1 mln di richieste è di $ 1.46. Considerando 10 mln di richieste è di $ 5.06. Considerando invece 100 mln si sale a $ 41.07. https://calculator.s3.amazonaws.com/index.html
* Si stava considerando la possibilità di fare del caching in locale su Unity per diminuire i prezzi di S3 ma mi sembra che i prezzi siano già sufficientemente bassi. Inoltre ci sarebbero i seguenti problemi
    * Ci sono almeno 5000 immagini e le carte sono sempre casuali, quindi perchè il caching sia efficiente l'utente deve giocare tante volte prima di reincontrare le stesse immagini
    * Se l'utente deve giocare tante volte vuol dire che deve scaricare tante immagini, quindi la cache occuperà tanto spazio (forse tutte le immagini occuperanno 5 GB)
    * A quel punto tanto vale far scaricare un pacchetto una tantum all'utente e non fargli utilizzare neanche S3. (Però questa opzione potrebbe non piacere) - UPDATE: confermo che questa opzione non piace a Eugenio, come era prevedibile

#### Altre feature

* Ho incontrato nel codice una Chat non implementata totalmente. Mi hanno detto che per il momento non c'è e probabilmente non sarà implementata per il lancio

### Client

* Il GameManager non è un GameManager ma gestisce un singolo match
* Vengono chiamati metodi asincroni su altre classi ma non vengono usate delle callback aspettare il risultato. Invece viene usato un anti-pattern molto brutto: il metodo corrente (che è una coroutine) si mette in Wait per aspettare che una variabile venga modificata dall'esterno. Questo anti-pattern viene utilizzato continuamente, ed è un casino da gestire.

```CSharp
public IEnumerator TakeCardsCoroutine(EventJSON[] events)
{
    ...

    NetworkManager.instance.TakeSprite(n.Immagine_400x400, i);
    
    ...

    yield return new WaitUntil(() => ctrlCard == 5);

    for (int a = 0; a < 5; a++)
    {
        ...
    }

    ...
}
```

* Il codice è pieno di roba schiantata
* Ci sono un bel po' di Singleton, ma non sono veri Singleton perchè non vengono istanziati in automatico. Sono solo oggetti che nell'awake settano una variabile statica con la propria istanza.
* Tanti anti-pattern
* Nel gioco ci sono diversi bug. In quel poco che l'ho provato ho riscontrato che
    * Spesso il gioco si pianta in una schermata di caricamento e l'unica cosa che si può fare è riavviarlo
    * Nei match a volte alcune carte non funzionano. In sostanza a fine partita non ti mostrano la loro descrizione
* Il repository è organizzato male. Il .gitignore non ignora le cose giuste (ci sono anche delle cose in Library committate) e per esempio mancano delle dll che dovrebbero essere versionate
* I campi pubblici dei MonoBehaviour a volte sono usati solo come variabili e rimangono non assegnati dall'inspector. Altre volte non sono assegnati perchè lo script viene utilizzato in diverse aree ma ogni volta vengono usati blocchi diversi.

![UIManager Inspector](./uimanager-inspector.png)

> Per esempio questo script viene usato per tutta la UI. Nella schermata del MainMenu sono assegnati solo i campi che si vedono sopra.

* La classe UiManager è un Singleton che però viene sovrascritto ogni volta che c'è un MonoBehaviour di quel tipo. Questa cosa capita anche con altre classi.

```CSharp

public class UiManager : MonoBehaviour
{
    public static UiManager instance;

    ...

    private void Awake()
    {
        instance = this;
        
        ...
    }

    ...
}
```

* Alcuni GameObject in hierarchy sono "inchiodati" a causa di codice del tipo: `categoryLeaderboard.transform.parent.parent.gameObject.SetActive(false);`.
* La password dell'utente viene salvata nei PlayerPrefs in chiaro
* Alcuni nomi di variabili non hanno proprio senso. Per esempio nel NetworkManager la password si chiama id. Poi alcune chiamate (per esempio l'autenticazione) usano dei parametri in modo improprio

```CSharp
a = PlayerPrefs.GetString("id", string.Empty);
b = PlayerPrefs.GetString("psw", string.Empty);
c = PlayerPrefs.GetString("plname", string.Empty);
...
NetworkManager.instance.playerID = a;
NetworkManager.instance.id = b;
NetworkManager.instance.playerName = c;
NetworkManager.instance.Authenticate(c, b, c, false);
SaveData(a, b, c);
```

> Qui Authenticate usa il nome dell'utente come id

* La logica segue sempre dei flussi molto strani. Per esempio la UI viene modificata direttamente da tutti in modo arbitrario

```CSharp
public class FakeLoadingStart : MonoBehaviour
{

    public CheckLogin obj;
    private IEnumerator Start()
    {
        UiManager.instance.loadingUi.SetActive(true);
        
        ...

        obj.Check();
    }
}

public class CheckLogin : MonoBehaviour
{

    public void Check()
    {
        if ( ... )
        {
            ...

            NetworkManager.instance.Authenticate(c, b, c, false);
            
            ...
        }
        else
            UiManager.instance.loadingUi.SetActive(false);
        
    }
}

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;
    ...
    public void Authenticate( ... )
    {
        ...
        socket.Emit("Authentication", JsonUtility.ToJson(new AuthenticationJSON(name, id, playerID, fb)));
        ...
    }
}
```

> Qui è veramente un casino perchè la schermata di caricamento viene attivata da FakeLoadingStart ma viene disattivata altrove. Dove? CheckLogin la disattiva solo in un caso; nell'altro caso il controllo passa al NetworkManager che però non la disattiva subito ma fa una chiamata al server senza gestirne direttamente il risultato. A questo punto può succedere qualunque cosa perchè il controllo passa al server che può anche fallire o fare cose diverse. Solo se va tutto a buon fine il server lancerà un evento sul socket che verrà gestito lato client (non so dove) e alla fine la schermata di caricamento sarà disattivata. Questo spiega perchè spesso il gioco si blocca in caricamento.

* L'attivaizione o la disattivazione degli oggetti più volte è gestita direttamente da eventi di bottoni in più punti della gerarchia. Sta cosa ovviamente è un casino. A volte si vede l'attivazione di tutta una barra di navigazione, piuttosto che di intere aree di UI. Alcuni fanno davvero tante cose....

![Button Event 1](button-event.png) ![Button Event 2](button-event2.png)

![Back Button Event](backbutton-event.png)

![Play Button](play-button.png)

* La gestione del gioco è un bordello. Non esiste un vero GameManager (c'è uno script che si chiama così ma fa un'altra cosa). Esiste un LevelManager che ha una variabile gameState che dovrebbe tenere traccia dello stato del gioco. Peccato che questo oggetto venga distrutto ad ogni cambio di scena quindi il gameState è sempre MainMenu. Per il resto il LevelManager carica le scene, ma fa anche cose che non c'entrano niente, come `ShowFacebookFriends` ¯\\_(ツ)_/¯
* Il NetworkManager non saprei come commentarlo. È come un grumo di spaghetti scotti del giorno prima che sono stati messi in frigo ancora caldi senza neanche un filo d'olio e sono stati successivamente riscaldati al microonde almeno un paio di volte. Se ne tiri via uno viene su tutto il piatto e forse anche il tavolo. Ora provo a spiegare perchè. No non ci riesco. È tutto sbagliato.
* Gli utenti di facebook vengono loggati usando il loro ID come password (dafuq)
* È pieno zeppo di codice non utilizzato che è anche difficile da capire perchè sono funzioni usate da altre funzioni usate da diverse altre funzioni che a loro volta non vengono utilizzate
* Le animazioni sono gestite con script custom che usano il plugin DoTween. Il problema è anche qui il flusso super incasinato, che passa tra il codice e la hierarchy in modo imprevedibile. Per esempio:

```CSharp
public class GameCard : MonoBehaviour, ...
{
    public void SetCardScore(int score)
    {
        ...

        shinyObject.SetActive(true);
        
        ...
    }
}
```

> Step 1: la classe GameCard attiva un GameObject "shinyObject"

![Step 2/A](animation-step2-a.png) ![Step 2/B](animation-step2-b.png)

> Step 2: a sinistra è evidenziato il GameObject referenziato da "shinyObject" all'interno della gerarchia della carta. A destra si vede lo script custom "TweenPositionOnAnimation" che viene utilizzato per far partire l'animazione durante l'OnEnable. Nella sezione "OnFinish" il secondo elemento fa partire un'altra animazione su un altro GameObject

![Step 3/A](animation-step3-a.png) ![Step 3/B](animation-step3-b.png)

> Step 3: L'oggetto in questione è evidenziato a sinistra. A destra si vede la sua animazione che in OnFinish fa un sacco di cose, tra cui far partire un'altra animazione dopo 0.3 secondi su un'altro oggetto.

![Step 4/A](animation-step4-a.png) ![Step 4/B](animation-step4-b.png)

> Step 4: Questa volta l'oggetto in questione si trova completamente in un altro punto della gerarchia. Anche lui chiama un'altra animazione di un'altro oggetto.

![Step 5/A](animation-step5-a.png) ![Step 5/B](animation-step5-b.png)

> Step 5: Siamo sempre fuori dalla carta, ancora su un altro oggetto della gerarchia. Idem come prima.


![Step 6/A](animation-step6-a.png) ![Step 6/B](animation-step6-b.png)

> Step 6: Ora si torna di nuovo nella gerarchia della carta. L'animazione attivata fa partire nuovamente un'altra animazione. Però mi sono rotto e non continuo perchè non so per quanto andrà avanti. Spero che a un certo punto questa catena finisca.

![Animation GIF](animations.gif)

> Ecco gli step di prima mostrati con una GIF che fa capire meglio il disagio.

* I PrefabVariant non vengono usati dove dovrebbero e ci sono anche una marea di riferimenti esterni ai prefab che rischiano di rompere cose

* Lo script delle carte (GameCard) è un bel po' incasinato per motivi già detti prima che non ripeto. In più esistono cose brutte di questo tipo:

```CSharp
    private void UpdateCardShadow()
    {
        shadow.effectDistance = new Vector2(0,
            startShadowDistance.y -
            (CustomMath.HorizontalDistance(notpositionedCard.rectTransform.position,
                 lineT.position) +
             Mathf.Clamp(
                 Mathf.Abs(CustomMath.VerticalDistance(notpositionedCard.rectTransform.position,
                     startPos)), 0, 100)) *
            shadowDimMultiplier);
        shadow.useGraphicAlpha = false;

        float alpha = Mathf.Clamp(startShadowColor.a -
                                  Mathf.Abs(
                                      (Mathf.Abs(CustomMath
                                                     .HorizontalDistance(
                                                         notpositionedCard
                                                             .rectTransform
                                                             .position,
                                                         lineT
                                                             .position) +
                                                 Mathf.Clamp(
                                                     Mathf.Abs(CustomMath.VerticalDistance(
                                                         notpositionedCard.rectTransform.position, startPos)), 0,
                                                     100)) *
                                       colorMultiplier)), -1.0f, 1.01f);
        alpha = Mathf.Abs(alpha);
//        Debug.Log("alpha: " + alpha);
        Color newColor = new Color(startShadowColor.r, startShadowColor.g, startShadowColor.b,
            dimensionCurve.Evaluate(alpha));
        shadow.effectColor = newColor;

    }
```

* Ho scoperto che il GameState passa da "mainMenu" a "inGame" ogni volta che viene ricevuta una carta dal server (questo è il modo in cui gli script sanno che si è in un match)
* Infine, un esempio di come le classi modifichino direttamente le variabili di altre classi (e poi perchè il GameManager che è un Singleton contiene un riferimento alla DropZone delle carte??)

```CSharp
public void Drag()
{
    if (LevelManager.instance.GameState == GameState.inGame)
    {
//            Debug.Log("vediamo");
        
        SetPositions(new Vector2(Input.mousePosition.x , Input.mousePosition.y));

        if (startParent != parentToReturn)
        {
            DropZone dropzone = GameManager.instance.dropZone;

            int siblingIndex = GameManager.instance.dropZone.transform.childCount;
            GameCard checkedCard;
            for (int i = 0; i < GameManager.instance.dropZone.transform.childCount; i++)
            {
                checkedCard = GameManager.instance.dropZone.transform.GetChild(i).GetComponent<GameCard>();
                if (checkedCard != null)
                {
                    if (transform.position.y > checkedCard.transform.position.y)
                    {
                        siblingIndex = i;

                        if (GameManager.instance.dropZone.PlaceholderSiblingindex < siblingIndex)
                            siblingIndex = GameManager.instance.dropZone.PlaceholderSiblingindex;
                        GameManager.instance.dropZone.PlaceholderSiblingindex = siblingIndex;

                        //break;
                    }
                    else
                    {
                        //siblingIndex = i - 1;


                        GameManager.instance.dropZone.PlaceholderSiblingindex = siblingIndex;
                    }
                }
            }

            //GameManager.instance.dropZone.PlaceholderSiblingindex = siblingIndex;
        }
    }
}
```

* E scusatemi ancora ma volevo farvi vedere anche questo

```CSharp
private IEnumerator TakeNetworkCard()
{
    //Debug.Log(LevelManager.instance.isForRes);
    if (!LevelManager.instance.isForRes)
    {
        UiManager.instance.EnableLoadingUi();
        haveToUpdatePoints = true;
        // if (FacebookScript.instance.gameId == 0)
//            Debug.Log("Domando");
        takeCard = false;
        //currentCategory = string.Empty;
        
        
        //Debug.Log(currentCategory);
//            Debug.Log("CCATEGORY: " + FacebookScript.instance.categorySelected);
        currentCategory = FacebookScript.instance.categorySelected;
        if(!string.IsNullOrEmpty(currentCategory))
            FacebookScript.instance.cat = currentCategory;
        FacebookScript.instance.categorySelected = "";
        StartCoroutine(TakePhotos());
        if (string.IsNullOrEmpty(currentCategory) || currentCategory == "0")
        {
            NetworkManager.instance.EmitMessage("GetCards",
                JsonUtility.ToJson(new NewPlayJSON(NetworkManager.instance.playerID,
                    FacebookScript.instance.opponentId, "Empty", "ITA")));
//                Debug.Log("Emitted");
        }
        else
        {
            NetworkManager.instance.EmitMessage("GetCards",
                JsonUtility.ToJson(new NewPlayJSON(NetworkManager.instance.playerID,
                    FacebookScript.instance.opponentId, currentCategory, "ITA")));
//                Debug.Log("Emitted with category");
        }

        
        NetworkManager.instance.alreadyExistingMatch = false;
        float startT = Time.time;
        /* else
            {
                WWWForm form = new WWWForm();
                form.AddField("matchid", FacebookScript.instance.gameId);
                NetworkManager.instance.TakeXml(Urls.GetUrl(Urls.getMatchEventsUrl), Urls.getMatchEventsQuery , form);
            }*/

        yield return new WaitWhile(() => !takeCard);
        
//            Debug.Log("Time to take cards: " + (Time.time - startT));

        //yield return new WaitForSeconds(0.5f);

        //XmlNodeList nodeList = NetworkManager.instance.returnedXmlNodeListFromTakeXml;



        yield return new WaitForEndOfFrame();


        UiManager.instance.gameUi.SetActive(true);


        yield return new WaitForEndOfFrame();

        UiManager.instance.categorySelectionUi.SetActive(false);
        LevelManager.instance.GameState = GameState.inGame;

        /*for (int j = 0; j < UiManager.instance.arrowParticle.Length; j++)
        {
            UiManager.instance.arrowParticle[j].Play();
        }*/
        if(MusicManager.instance != null)
            MusicManager.instance.PlayGame();
        ActiveFirstCardDescription();
    }
    else
    {
        // LevelManager.instance.isForRes = true;
        UiManager.instance.EnableLoadingUi();
        UiManager.instance.CheckIfTurn();
    }
}
```

Una considerazione finale sulla parte di client. Se dovessimo implementare le API RESTful al posto dei socket, significa che dovremmo rifare totalmente il network del client, che però è super incastrato nelle altre classi. Questa cosa mi spaventa molto perchè significa andare necessariamente a toccare la logica del gioco. Quasi tutte le parti del gioco infatti rimangono in attesa delle risposte dal server in modo completamente indipendente dalle richieste che sono state effettuate. Invece in REST è tutto più ordinato perchè una richiesta al server prevede sempre una risposta che viene gestita nello stesso "luogo" all'interno del codice.

### Studio sui socket

Ho fatto una ricerca per capire se i socket sono effettivamente un problema. Apparentemente nel nostro caso non lo sono. I socket sono a basso livello dei descrittori di file quindi il numero di connessioni dipende da quanti file può tenere aperti in contemporanea il sistema operativo. Inoltre questo numero è solo una configurazione che può essere cambiata.

Diciamo che il problema c'è in casi normali, ovvero quando i socket vengono usati in modo intensivo: in quel caso il limite di connessioni è dato dall'uso intensivo delle risorse e della banda. Nel nostro caso sembra che usare socket o RESTful api non dovrebbe cambiare più di tanto perchè comunque si tratta di comunicazioni non intensive.

Ulteriori informazioni:

* [Ottimo articolo su socket.io benchmarking](http://drewww.github.io/socket.io-benchmarking/)