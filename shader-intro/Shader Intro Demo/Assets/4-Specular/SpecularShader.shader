﻿Shader "Demo/SpecularShader"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
        _SpecularAmount ("Specular Amount", Range(0.0, 1.0)) = 1.0
        _SpecularPower ("Specular Power", Range(1.0, 100.0)) = 25.0
        _SpecularColor ("Specular Color", Color) = (1.0, 1.0, 1.0, 0.0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct vInput
            {
                float4 position : POSITION;
                float4 normal   : NORMAL;
                float2 uv       : TEXCOORD0;
            };

            struct v2f
            {
                float4 position : SV_POSITION;
                float2 uv       : TEXCOORD0;
                float3 normal   : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4    _MainTex_ST;
            float     _SpecularAmount;
            float     _SpecularPower;
            float4    _SpecularColor;

            v2f vert (vInput i)
            {
                v2f o;
                o.position = UnityObjectToClipPos(i.position);
                o.normal   = UnityObjectToWorldNormal(i.normal);
                o.uv       = i.uv * _MainTex_ST.xy + _MainTex_ST.zw;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 n = normalize(i.normal);
                float3 l = normalize(UnityWorldSpaceLightDir(i.position));
                float3 v = normalize(UnityWorldSpaceViewDir(i.position));
                float3 r = normalize(reflect(l, n));
                float  d = dot(l, n);

                float diffuse = saturate(d);
                float specular = saturate(_SpecularAmount * pow(dot(r,v), _SpecularPower) * (d > 0.0)); // saturate(_SpecularAmount * pow(dot(r, v), _SpecularPower));
                
                float4 color = tex2D(_MainTex, i.uv); 
                
                color.xyz *= diffuse;
                color.xyz += (specular);
                return color;
            }
            ENDCG
        }
    }
}
