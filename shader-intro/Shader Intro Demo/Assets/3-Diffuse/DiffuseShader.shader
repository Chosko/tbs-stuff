﻿Shader "Demo/DiffuseShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct vInput
            {
                float4 position : POSITION;
                float4 normal   : NORMAL;
                float2 uv       : TEXCOORD0;
            };

            struct v2f
            {
                float4 position : SV_POSITION;
                float2 uv       : TEXCOORD0;
                float3 normal   : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4    _MainTex_ST;

            v2f vert (vInput i)
            {
                v2f o;
                o.position = UnityObjectToClipPos(i.position);
                o.normal   = UnityObjectToWorldNormal(i.normal);
                o.uv       = i.uv * _MainTex_ST.xy + _MainTex_ST.zw;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 lightDirection = UnityWorldSpaceLightDir(i.position);
                float diffuseAmount = dot(lightDirection, normalize(i.normal));
                float4 color = tex2D(_MainTex, i.uv); 
                color.xyz *= dot(lightDirection, normalize(i.normal));
                return color;
            }
            ENDCG
        }
    }
}
