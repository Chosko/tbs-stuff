﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SpaceDistortion : MonoBehaviour
{
    [Range(-2, 2)]
    public float distortionAmount = 0;

    private int _DistortionOrigin_ID;
    private int _DIstortionAmount_ID;
                               
    private void Start ()      
    {                          
        _DistortionOrigin_ID = Shader.PropertyToID ("_DistortionOrigin");
        _DIstortionAmount_ID = Shader.PropertyToID ("_DistortionAmount");
    }                          
                               
    void Update()
    {
        Shader.SetGlobalVector (_DistortionOrigin_ID, transform.position);
        Shader.SetGlobalFloat (_DIstortionAmount_ID, distortionAmount);
    }                          
}                              
                               