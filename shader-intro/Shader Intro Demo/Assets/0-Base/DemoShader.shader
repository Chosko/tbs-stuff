﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Demo"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct vInput
            {
                float4 objPosition : POSITION;
                float2 uv          : TEXCOORD0;
                float4 objNormal   : NORMAL;
            };

            struct v2f
            {
                float4 clpPosition : SV_POSITION;
                float2 uv          : TEXCOORD0;
                float4 wldNormal   : TEXCOORD1;
                float4 wldPosition : TEXCOORD2;
            };

            sampler _MainTex;
            float4  _MainTex_ST;

            v2f vert (vInput i)
            {
                v2f o;
                o.clpPosition = UnityObjectToClipPos(i.objPosition); 
                o.wldNormal   = mul(i.objNormal, unity_WorldToObject);
                o.wldPosition = mul(unity_ObjectToWorld, i.objPosition);
                o.uv = _MainTex_ST.zw + i.uv * _MainTex_ST.xy;
                return o;
            }

            float4 frag (v2f i) : SV_Target
            {
                float3 n = normalize(i.wldNormal);
                float3 l = normalize(UnityWorldSpaceLightDir(i.wldPosition));
                float d = saturate(dot(l, n));
                
                float3 color = tex2D(_MainTex, i.uv).rgb;
                return float4(color * d,  1.0);
            }
            ENDCG
        }
    }
}
