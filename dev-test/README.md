# Tiny Bull Studios Dev Test

The aim of this test is to modify an existing project, adding new features and improving the existing ones.

The deadline is 3 days from now. You will submit your modified Unity project along with a playable standalone build.

## Selection criteria

While working on this project keep in mind that:

* Some features are more important. They should be implemented first.
* Other features are optional. Focus on them only when you are done with the main ones.
* We prefer quality over quantity. It's better to send a well-designed project with only the main features than a poorly designed one with more optional features.
* If you are running out of time, don't rush. Complete only the feature you are working on as well as you can.
* Meeting the deadline is crucial. When the time is out, submit whatever you have done.

## Getting started

1. Download and install Unity 2018 (the latest available version)
2. Create a new Unity Project, named TBS-DevTest
3. Download and import [the Space Shooter Tutorial](https://unity3d.com/learn/tutorials/s/space-shooter-tutorial)
4. Download and import [the TBS package](http://example.org) with all the additional assets needed for this test
5. Start coding!

## Main Features

### Player health

In the basic tutorial the game ends as soon as the player takes damage. Implement a new feature to achieve the following:

* The player has a health value, displayed with a bar.
* When the player takes damage, it is not destroyed unless the health reaches zero.
* Each damage taken reduces the health by 1/3.
* The game ends when the player is destroyed.
* The health bar is shown in a corner of the screen
    * Use the assets provided in the TBS package
    * The bar must be filled according to the player's current health

### Power-ups

When an enemy ship is destroyed it may drop a power-up, with a 15% chance. Power-ups can be collected by moving the player's ship on them. When collected, a power-up disappears from the screen and its effect is activated. There are three different power-ups, which are chosen randomly.

* Health
    * It is displayed with the asset *health-powerup.png*
    * When collected, the sound *health-powerup.wav* is played
    * When collected, the player recovers 1/3 of their health, without exceeding the maximum value
* Shield
    * It is displayed with the asset *shield-powerup.png*
    * When collected, the sound *shield-powerup.wav* is played
    * When collected, the player's ship receives a shield which protects it from the next hit
    * The shield is displayed as a ball around the player, using the asset *shield.png*
    * When the shield takes damage it disappears
    * When the shield disappears, the sound *shield-disappearing.wav* is played
    * When the shield is enabled and the player collects another shield power-up, nothing happens.
* Homing missiles
    * It is displayed with the asset *homing-powerup.png*
    * When collected, the sound *homing-powerup.wav* is played
    * When collected, an additional weapon is enabled as secondary fire
    * The new weapon automatically fires a homing missile every 5 seconds
    * When the missile is fired, the sound *homing-fire.wav* is played
    * The effect of this power-up lasts for 20 seconds
    * The homing missiles behave as follows
        * when fired, they start following the closest enemy ship
        * when there are no enemies on the screen, they keep moving in a straight line
        * they explode when colliding with an enemy ship or an asteroid, inflicting damage to it
    * When this power-up is active and the player collects another *homing missile* power-up, the duration of this power-up is increased by 20 seconds

### Stronger enemies

Add a new type of enemy ship that is spawned like the existing ones.

* Use the same model and assets of the standard enemy ships.
* This ship will have double health.
* When at full health the ship's color is red.
* When damaged for the first time, the following will happen.
    * The ship doesn't explode.
    * The color of the ship turns to the default color (the same of standard enemy ships).
* When damaged a second time, the ship explodes.

## Bonus Features (optional)

If you complete the main features ahead of time, you can choose to implement one or more of the following features. The aim is to prove that you are capable of solving a more generic problem, while expressing specific skills.

### Graphics

Add an effect that better shows the health status of the player ship. For instance, you can add a smoke effect to the player's ship when it is heavily damaged.

### AI

Add an enemy kamikaze ship that follows the player, trying to avoid bullets and asteroids.

### UI

Create a Main Menu that shows a background image, the game logo, a "New Game" button and an "Exit" button.

You can use the assets provided in the TBS package.

### Audio

Add the following keyboard controls:

* O: decrease SFX volume
* P: increase SFX volume
* K: decrease Music volume
* L: increase Music volume
* M: mute/unmute all

### Save/Load data from file

Keep the top 5 scores in a file and show them on the Game Over screen.

### Version control

Develop this project using git and upload it to github or bitbucket.
