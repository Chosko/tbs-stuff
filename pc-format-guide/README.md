# PC Format Guide

## Windows installation

Make a bootable USB drive using the Windows Media Creation Tool.

When installing Windows keep in mind the following:

* We use **Windows 10 PRO**
* The **language** settings are the following:
    * Language to install: **English (United Kingdom)**
    * Time and currency format: **Italian (Italy)**
    * Keyboard or input method: **Italian**
* The product key insertion must be skipped (Windows will be activated later)
* We always want a **Custom Install** that allows to select the HD.
* Windows must be installed on the main HD, which usually is the **SSD**.
* Make sure not to format the other drives
* When asked, set up for **personal use**
* Log in with **tinybullstudios@outlook.com** (Ask Matteo Lana to insert the password)
* Set the **PIN** as the number of the PC, with 4 digits. For instance, the PIN for PC-07 is 0007

## Programs installation

All the programs should be installed in **C:**

Please remove the installers from the Download folder when you have finished.

### Basic programs

These programs must be installed on all the TBS PCs

* Chrome
* Nvidia Experience (if the graphics card is nvidia)
* Twist
* Telegram
* TeamViewer
    * Select "Installation to acces this computer remotely"
    * Select "Personal / Non-commercial use"
    * Follow the Unattended Access configuration wizard
        * Computer name: `TBS-XX`
        * Password: `TeamViewer-TBS-XX`
        * Select "I don't want to create a TeamViewer account now"
        * Copy the TeamViewer ID and paste it to the end of this guide
* 7Zip 64-bit
* Notepad++ 64-bit
* Visual Studio Code, with the following extensions
    * GitLens
    * EditorConfig for VS Code

After installing all these programs, please remove the installers from the Download folder.

### Git

* Install SourceTree
    * **Flag** **Git** and **Unflag** **Mercurial**
    * Login with any user
    * Don't setup a default Nickname and Password for Git
* Logout from SourceTree: Tools > Options > Authentication. Select the account and delete it
* Install [Git For Windows 64-bit](https://git-scm.com/download/win)
    * Keep the default components (make sure "Git LFS (Large File Support)" is selected)
    * Use Notepad++ as Git's default editor
    * Use Git from Git Bash only
    * Use the OpenSSL library
    * Checkout Windows-style, commit Unix-style line endings
    * Use MinTTY
    * Keep all the other settings unchanged
* Configure SourceTree: Tools > Options > Git > Git Version - Select **System**

#### Configure Git

Edit `C:\Users\tinyb\.gitconfig` with Notepad++ (don't use Microsoft Notepad) and overwrite the content with the following

```
[core]
        autocrlf = true
        editor = 'C:/Program Files/Notepad++/notepad++.exe' -multiInst -notabbar -nosession -noPlugin
[user]
        name = Username
        email = username@tinybullstudios.com
[filter "lfs"]
        smudge = git-lfs smudge -- %f
        process = git-lfs filter-process
        required = true
        clean = git-lfs clean -- %f
[diff]
        tool = code
[merge]
        tool = code
        conflictstyle = diff3
[mergetool "code"]
        cmd = code --wait $MERGED
[difftool "code"]
        cmd = code --wait --diff $LOCAL $REMOTE
[mergetool]
        prompt = false
```

Then change **Username** and **username@tinybullstudios.com** with the Nick and Email that you want to associate to the commits made on this PC (For instance, `Chosko` and `ruben.caliandro@gmail.com`).

#### Configure SSH

Open the Git Bash (search "Git Bash" in the Start Menu) and type the following, substituting `XX` with the number of the PC (for instance `rsa-key-TBS-07`)

```bash
ssh-keygen -t rsa -b 4096 -C "rsa-key-TBS-XX"
```

When you're prompted to "Enter a file in which to save the key" press Enter. This accepts the default file location.

```
Enter a file in which to save the key (/c/Users/tinyb/.ssh/id_rsa): [Press enter]
```

When you're prompted to enter the passphrase, type `SSH-passphrase-TBS-XX`, substituting `XX` with the number of the PC.

```
Enter passphrase (empty for no passphrase): [Type SSH-passphrase-TBS-XX]
Enter same passphrase again: [Type the same passphrase]
```

Now type the following

```bash
echo "source ~/.bash_profile" > ~/.bashrc
code ~/.bash_profile
```

The last command will open Visual Studio Code with a newly created file, named .bash_profile

Paste the following code

```bash
alias gl='git log --pretty=format:"%C(blue normal bold)%<(15,trunc)%an%Creset %C(yellow)(%cr)%Creset%C(auto)%d%Creset %C(dim)%<(100,mtrunc)%s%Creset %Cred%H%Creset %n%-b%Creset" --graph'
alias gla='git log --pretty=format:"%C(blue normal bold)%<(15,trunc)%an%Creset %C(yellow)(%cr)%Creset%C(auto)%d%Creset %C(dim)%<(100,mtrunc)%s%Creset %Cred%H%Creset %n%-b%Creset" --all --graph'
alias gld='git log --pretty=format:"%C(blue normal bold)%<(15,trunc)%an%Creset %C(yellow)(%cr)%Creset%C(auto)%d%Creset %C(dim)%<(100,mtrunc)%s%Creset %Cred%H%Creset %n%-b%Creset" --graph --date-order'
alias glad='git log --pretty=format:"%C(blue normal bold)%<(15,trunc)%an%Creset %C(yellow)(%cr)%Creset%C(auto)%d%Creset %C(dim)%<(100,mtrunc)%s%Creset %Cred%H%Creset %n%-b%Creset" --all --graph --date-order'
 
env=~/.ssh/agent.env
 
agent_load_env () { test -f "$env" && . "$env" >| /dev/null ; }
 
agent_start () {
    (umask 077; ssh-agent >| "$env")
    . "$env" >| /dev/null ; }
 
agent_load_env
 
# agent_run_state: 0=agent running w/ key; 1=agent w/o key; 2= agent not running
agent_run_state=$(ssh-add -l >| /dev/null 2>&1; echo $?)
 
if [ ! "$SSH_AUTH_SOCK" ] || [ $agent_run_state = 2 ]; then
    agent_start
    ssh-add
elif [ "$SSH_AUTH_SOCK" ] && [ $agent_run_state = 1 ]; then
    ssh-add
fi
 
unset env
```

Save the file and close Visual Studio Code. Then close and reopen the Git Bash.

From now on, every time you open the Git Bash for the first time you will be prompted to enter the passphrase. Type it (`SSH-passphrase-TBS-XX`). This will enable you to push and pull from all the Tiny Bull Studios' repositories.

Type

```bash
cat ~/.ssh/id_rsa.pub
```

Copy the full output and send it to the thread "SSH Keys" on Twist (channel Programmer)

Now import the SSH key pair on SourceTree 

* Open SourceTree
* Tools > Create or Import SSH Keys > Conversions > Import key
* Select the key, which is located at `C:\Users\tinyb\.ssh\id_rsa`
* Enter the passphrase (`SSH-passphrase-TBS-XX`)
* Click on "Save private key" and save to `C:\Users\tinyb\.ssh\putty_id_rsa.ppk`, then Close the generator
* Tools > Options > General > SSH Client Configuration > SSH Key
* Select `C:\Users\tinyb\.ssh\putty_id_rsa.ppk`
* SSH Client: `Putty / Plink`
* Ensure `Automatically start SSH agent when Sourcetree opens` is flagged
* Close SourceTree and reopen it

From now on, the first time you open SourceTree you will be prompted to enter the passphrase.

> To clone repositories you must now use the SSH URLs, for instance `git@bitbucket.org:tinybullstudios/palio.git`. If you use the HTTPS URL instead (`https://bitbucket.org/tinybullstudios/palio.git`), git will always prompt you to enter your Bitbucket username and password.

#### Clone main repositories

The main repositories should be cloned in a way that the working directory is located in `C:\` and the git directory is located in `D:\` (or any other secondary HD)

* Create the following directories
    * `C:\UnityProjects`
    * `D:\UnityProjects\GitFolders`
* Clone the main repositories using the following commands

```bash
cd /c/UnityProjects
git clone --separate-git-dir="/d/UnityProjects/GitFolders/<repo-name>_git" git@bitbucket.org:tinybullstudios/<repo-name>.git <repo-name>
```

In the last command, `<repo-name>` should be substituted with the name of the repo, which is already in the URL. For instance:

```bash
git clone --separate-git-dir="/d/UnityProjects/GitFolders/palio_git" git@bitbucket.org:tinybullstudios/palio.git palio
```

Then, intialize git-flow 

```bash
git flow init
```

This command prompts you to enter some additional info. Just press enter every time without insert anything.

## Unity

* Unity Hub
    * Ask Matteo Lana to insert the login credentials
* Unity 2018.3 - the latest version.

After installing Unity open all the projects to make the first import.

## Only for Blind programmers

* Clone Blind and Blind_PS4
* Oculus
* Steam
* Unity 5.6
* All the PS4 stuff
* FMOD x1

## Only for artists

* Marvellous Designer (VMWare)
* ZBrush (VMWare)
* Maya LT x3
* Substance x1
* Photoshop x1

## TBS TeamViewer IDs

* PC-07: 1 108 100 949

Password: `TeamViewer-TBS-XX`